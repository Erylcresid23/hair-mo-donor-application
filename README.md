# Hair Mo Donor Mobile application - an online hair supply information and communication platform 

With the rapid growth of social media sites around the world, the number of requests for hair donors has increased; they reach out through Facebook and Twitter to request hair donations. The struggles of an individual fighting cancer is draining, not just physically but emotionally and mentally. Most cancer patients are unable to purchase a wig because to a lack of funds; a human hair wig costs a lot of money when purchased in a parlor shop.

Numerous  individuals  are about developing their hair since they need to sell it to parlors. But these days, we ought to increment the number of people with great hearts who are willing to give their hair for free. It's not about making a gift, but about making a difference. The main goal of application is to educate the community about the benefits and importance of hair donation.

As a result, the proponents intends to design and develop Hair Mo! Is a supply information and communication  platform that provide the most convenient method for better hair donation transactions. Both donors and patients will benefit from the use of this application. User conduct smooth transactions within this application because of features of it, that will be discuss later through demo. 

It also encourages other organizations to make hair donations. Thus, charities and foundations can innovate the use of technology throughout transactions with donors to bridge the gap between ICT innovations and different organization
