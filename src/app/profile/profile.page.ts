import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: 'profile.page.html',
  styleUrls: ['profile.page.scss']
})
export class ProfilePage {

  constructor(public alertController: AlertController) {}

  ngOnit(){
    
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Hair Mo',
      message: 'Profile Updated.',
      buttons: ['OK']
    });

    await alert.present();
    let result = await alert.onDidDismiss();
    console.log(result);

  }


}
