import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IonContent } from '@ionic/angular';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {
  @ViewChild('IonContent', { static: true }) content: IonContent
  paramData: any;
  msgList: any;
  userName: any;
  user_input: string = "";
  User: string = "Me";
  toUser: string = "user";
  start_typing: any;
  loader: boolean;


  constructor(public activRoute: ActivatedRoute) {
    this.activRoute.params.subscribe((params) => {
      // console.log(params)
      this.paramData = params
      this.userName = params.name
    });
    this.msgList = [
      {
        userId: "user",
        userName: "user",
        userAvatar: "../../assets/chat/chat4.jpg",
        time: "10:00",
        message: "Hello are you?",
        id: 0
      },
      {
        userId: "Me",
        userName: "Me",
        userAvatar: "../../assets/chat/chat5.jpg",
        time: "12:03",
        message: "Im great, thank you for accepting my request. ",
        id: 1,
      },
      {
        userId: "user",
        userName: "user",
        userAvatar: "../../assets/chat/chat4.jpg",
        time: "10:03",
        message: "your welcome, lets call",
        id: 3,
      },
      {
        userId: "Me",
        userName: "Me",
        userAvatar: "../../assets/chat/chat5.jpg",
        time: "12:06",
        message: "sure",
        id: 4
      },
      {
        userId: "user",
        userName: "user",
        userAvatar: "../../assets/chat/chat4.jpg",
        time: "10:08",
        message: "maybe later",
        id: 5
      }
    ];
  }

  ngOnInit() {
  }
  sendMsg() {
    if (this.user_input !== '') {
      this.msgList.push({
        userId: this.toUser,
        userName: this.toUser,
        userAvatar: this.paramData.image ? this.paramData.image : "../../assets/chat/chat4.jpg",
        time: "12:01",
        message: this.user_input,
        id: this.msgList.length + 1
      })
      this.user_input = "";
      this.scrollDown()
      setTimeout(() => {
        this.senderSends()
      }, 500);

    }
  }
  senderSends() {
    this.loader = true;
    setTimeout(() => {
      this.msgList.push({
        userId: this.User,
        userName: this.User,
        userAvatar: "../../assets/chat/chat5.jpg",
        time: "10:15",
        message: "Send me the details of your shipping address, Please!.."
      });
      this.loader = false;
      this.scrollDown()
    }, 2000)
    this.scrollDown()
  }
  scrollDown() {
    setTimeout(() => {
      this.content.scrollToBottom(50)
    }, 50);
  }

  userTyping(event: any) {
    // console.log(event);
    this.start_typing = event.target.value;
    this.scrollDown()
  }
}