import { Component } from '@angular/core';
import { ɵNgNoValidate } from '@angular/forms';

@Component({
    selector: 'app-feeds',
    templateUrl: 'feeds.page.html',
    styleUrls: [ 'feeds.page.scss' ]
})
export class FeedsPage {

    newsfeedList = [
        {
            userName: 'Eryl',
            userImage: './assets/images/p1.jpg',
            userPlace: 'Beneficiaries',
            userPost: 'Please! I am looking for hair donor location: Batangas City.'
        },
        {
            userName: 'Tin',
            userImage: './assets/images/p2.jpg',
            userPlace: 'Donor',
            userPost: 'Hello, I am donor, I am willing to donate my hair for free. location: Malitam Batangas City.'
        },
        {
            userName: 'JA',
            userImage: './assets/images/p7.jpg',
            userPlace: 'Beneficiaries',
            userPost: 'Hello, We are looking for my daughter hair donor, please help us! location: Calicanto Batangas City.'
        }
        
    ];

    slideOpts = {
        initialSlide: 0,
        speed: 400
    };

    constructor( ) { }
 
  ngOnit(){
  }



}