import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-post-status',
  templateUrl: './post-status.page.html',
  styleUrls: ['./post-status.page.scss'],
})
export class PostStatusPage implements OnInit {

  constructor(private alertController: AlertController) { }

  public form = {
    status: ""
    
  }
  print(){
    console.log(this.form)
  }

  ngOnInit() {
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Hair Mo',
      message: 'Are you sure you want to post these?.',
      buttons: ['Yes','Cancel']


    });

    await alert.present();
    let result = await alert.onDidDismiss();
    console.log(result);

  }
}
