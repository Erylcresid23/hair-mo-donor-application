import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-newpost',
  templateUrl: 'newpost.page.html',
  styleUrls: ['newpost.page.scss']
})
export class NewpostPage {

  requeststatusList = [
      {
          userName: 'Eryl',
          userImage: './assets/images/p1.jpg',
          userPlace: '2d ago',
          userPost: 'If you are looking for a hair donor with a hair lenghr of 10cm. Feel free to contact me at 0913123123'
      },
      {
          userName: 'Eryl',
          userImage: './assets/images/p2.jpg',
          userPlace: '2w ago',
          userPost: 'Hello, I am donor, I am willing to donate my hair for free. location: Malitam Batangas City.'
      }
  ];

  slideOpts = {
      initialSlide: 0,
      speed: 400
  };

  constructor(public alertController:AlertController) {
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Hair Mo',
      message: 'Are you sure you want to delete these post?.',
      buttons: ['Yes','Cancel']
    });

    await alert.present();
    let result = await alert.onDidDismiss();
    console.log(result);

  }

}
