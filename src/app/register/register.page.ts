import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage{

  constructor(private alertController: AlertController ) { }
 
  ngOnit(){
  }
  public form = {
    fullname: "",
    username: "",
    address: "",
    contactnumber: "",
    age: "",
    type: "",
    password: ""
  }
  print(){
    console.log(this.form)
  }


    async presentAlert() {
      const alert = await this.alertController.create({
        header: 'Hair Mo',
        message: 'Account Created.',
        buttons: ['OK']
  
  
      });
  
      await alert.present();
      let result = await alert.onDidDismiss();
      console.log(result);
  
    }
  }

 


