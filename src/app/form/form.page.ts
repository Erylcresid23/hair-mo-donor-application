import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-form',
  templateUrl: './form.page.html',
  styleUrls: ['./form.page.scss'],
})
export class FormPage implements OnInit {

  constructor(public alertController:AlertController) { }
  
  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Hair Mo',
      message: 'Request Submitted, thank you!',
      buttons: ['OK']


    });

    await alert.present();
    let result = await alert.onDidDismiss();
    console.log(result);

  }
  async presentAlert2() {
    const alert = await this.alertController.create({
      header: 'Hair Mo',
      message: 'Are you sure you want to cancel request?',
      buttons: ['Yes','No']


    });

    await alert.present();
    let result = await alert.onDidDismiss();
    console.log(result);

  }

  ngOnInit() {
  }

}
