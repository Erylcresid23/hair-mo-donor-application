import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
    selector: 'app-search',
    templateUrl: 'search.page.html',
    styleUrls: [ 'search.page.scss' ]
})
export class SearchPage {

    userrequestList = [
        {
            userName: 'Eryl',
            userImage: './assets/images/p1.jpg',
            userPlace: 'Sta.Rita Karsada Batangas City',
            userPost: 'Please! I am looking for hair donor location: Batangas City.'
        },
        {
            userName: 'Tin',
            userImage: './assets/images/p2.jpg',
            userPlace: 'Donor',
            userPost: 'Hello, I am donor, I am willing to donate my hair for free. location: Malitam Batangas City.'
        },
        {
            userName: 'JA',
            userImage: './assets/images/p3.png',
            userPlace: 'Beneficiaries',
             userPost: 'Hello, We are looking for my daughter hair donor, please help us! location: Calicanto Batangas City.'
        }
        
    ];

    slideOpts = {
        initialSlide: 0,
        speed: 400
    };

    constructor(public alertController: AlertController ) { }
 
  ngOnit(){
  }
        async presentAlert() {
            const alert = await this.alertController.create({
              header: 'Hair Mo',
              message: 'Are you sure you want to accept these request?.',
              buttons: ['Yes','Cancel']
        
        
            });
        
            await alert.present();
            let result = await alert.onDidDismiss();
            console.log(result);
        
          }
        
        async presentAlert2() {
            const alert = await this.alertController.create({
              header: 'Hair Mo',
              message: 'Are you sure you want to delete these request?.',
              buttons: ['Yes','Cancel']
            });
        
            await alert.present();
            let result = await alert.onDidDismiss();
            console.log(result);
        
          }
        }
    
