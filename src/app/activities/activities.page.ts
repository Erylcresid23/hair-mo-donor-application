import { Component } from '@angular/core';

@Component({
    selector: 'app-activities',
    templateUrl: 'activities.page.html',
    styleUrls: [ 'activities.page.scss' ]
})
export class ActivitiesPage {

    notifByTime = [
        {
            time: 'Last Week',
            notification: [
                {
                    imageUrl: './assets/images/p4.jpg',
                    userDetail: 'Ototu',
                    description: 'Accept your request.',
                    reason:'Please contact me at 0912345678. Thank You!'
                },
                {
                    imageUrl: './assets/images/p5.png',
                    userDetail: 'JA',
                    description: 'Declined your request.',
                    reason:'Reason: Already found a Donor. Thank You!'
                }
            ]
        },
        {
            time: 'Last Month',
            notification: [
                {
                    imageUrl: './assets/images/p1.jpg',
                    userDetail: 'Tin',
                    description: 'Declined your request.',
                    reason:'Reason: Already found a Donor. Thank You!'
                },
                {
                    imageUrl: './assets/images/p2.jpg',
                    userDetail: 'Kysler',
                    description: 'Declined your request.',
                    reason:'Reason: Im not looking for donor anymore. Thank You!'
                }
            ]
        },
        {
            time: 'Earlier',
            notification: [
                {
                    imageUrl: './assets/images/p6.jpg',
                    userDetail: 'Cresid',
                    description: 'Declined your request.',
                    reason:'Im Interested, contact me at 0912345678. Thank You!'
                }
            ]
        }
    ];

    constructor() {
    }

}
